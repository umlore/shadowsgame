﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridObjectData : MonoBehaviour
{
    // Start is called before the first frame update

    public string symbol;
    public bool passthru;
    public Vector2Int gridPos;

    private GridManager gm;

    void Start() {
        gm = GameObject.FindObjectOfType<GridManager>();
        //and on the first day...
        gm.addGod(this);
    }
}
