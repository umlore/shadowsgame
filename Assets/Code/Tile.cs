﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tile : MonoBehaviour
{
    private List<GridObjectData> objectStack = new List<GridObjectData>();

    private TextMeshProUGUI _text;
    private TextMeshProUGUI text {
        get {
            if (_text == null) {
                _text = GetComponentInChildren<TextMeshProUGUI>();
            }
            return _text;
        }
    }

    void Start() {
        updateText();
    }

    private void updateText() {
        text.SetText(getTopText());
    }

    public void addGodTop(GridObjectData tile) {
        objectStack.Insert(0, tile);
        updateText();
    }

    public void removeGod(GridObjectData god) {
        objectStack.Remove(god);
        updateText();
    }

    //return text of object at front of objectStack
    public string getTopText() {
        if (objectStack.Count > 0) {
            return objectStack[0].symbol;
        }
        //if nothing on tile
        return "";
    }

    public bool isPassable() {
        //if any object on tile is impassible, false, else true
        foreach (GridObjectData god in objectStack) {
            if (!god.passthru) {
                return false;
            }
        }
        return true;
    }
}
