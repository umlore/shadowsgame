﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GridManager : MonoBehaviour
{

    public Grid grid;


    public GameObject tile;
    const int xgridsize = 20;
    const int ygridsize = 10;
    public Tile[][] tiles = new Tile[xgridsize][];

    // Start is called before the first frame update
    void Awake()
    {
        grid = GetComponent<Grid>();

        //create columns of tiles
        for (int x = 0; x < xgridsize; x++) {
            tiles[x] = new Tile[ygridsize];

            //create tiles within columns
            for (int y = 0; y < ygridsize; y++) { 
                tiles[x][y] = Instantiate(tile, grid.GetCellCenterWorld(new Vector3Int(x, y, 0)), Quaternion.identity).GetComponent<Tile>();
            }
        }
    }

    public void addGod(GridObjectData god) {
        tiles[god.gridPos.x][god.gridPos.y].addGodTop(god);
    }

    //remove gridobjectdata from grid
    public void removeGod(GridObjectData god) {
        tiles[god.gridPos.x][god.gridPos.y].removeGod(god);
    }

    public void moveTo(Vector2Int newPos, GridObjectData god) {
        
        //am i in bound of the grid?
        if (!(newPos.x < xgridsize && newPos.x >= 0 && newPos.y < ygridsize && newPos.y >= 0)) {
            return; //todo: return something?
        }

        //is something impassable at newPos?
        if (!tiles[newPos.x][newPos.y].isPassable()) {
            return;
        }

        //if we pass all our canMove checks, actually move
        removeGod(god);
        tiles[newPos.x][newPos.y].addGodTop(god);
        god.gridPos = newPos;
    }
}
