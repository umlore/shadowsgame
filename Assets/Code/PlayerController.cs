﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public Grid grid;

    private float timeTilNextMove = 0;
    private const float timeBetweenMoves = .2f;

    //component references
    private GridObjectData playerGod;
    private GridManager gm;

    // Start is called before the first frame update
    void Start()
    {
        playerGod = GetComponent<GridObjectData>();
        gm = grid.GetComponent<GridManager>();
    }

    // Update is called once per frame
    void Update()
    {
        timeTilNextMove -= Time.deltaTime;

        if (timeTilNextMove <= 0) {
            if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 1f) {
                Vector2Int newPos = new Vector2Int(playerGod.gridPos.x + (int)Input.GetAxisRaw("Horizontal"), playerGod.gridPos.y);
                gm.moveTo(newPos, playerGod);
                timeTilNextMove = timeBetweenMoves;
            } else if (Mathf.Abs(Input.GetAxisRaw("Vertical")) == 1f) {
                Vector2Int newPos = new Vector2Int(playerGod.gridPos.x, playerGod.gridPos.y + (int)Input.GetAxisRaw("Vertical"));
                gm.moveTo(newPos, playerGod);
                timeTilNextMove = timeBetweenMoves;
            }
        }
    }
}
